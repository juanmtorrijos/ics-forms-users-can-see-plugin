<?php

add_action( 'personal_options_update', 'ics_forms_save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'ics_forms_save_extra_user_profile_fields' );

function ics_forms_save_extra_user_profile_fields( $user_id ) {
  $saved = false;
  if ( current_user_can( 'edit_user', $user_id ) ) {
    update_user_meta( $user_id, 'icsc_classification', $_POST['icsc_classification'] );
    $saved = true;
  }
  return true;
}