<?php

/*
Plugin Name: ICS Class Forms Users Can See
Description: This Plugin Creates the Funcitonality that Enables New Suveryors to See or Hide the Forms and Documentation According to their Capabilities.
Version:     20160410
Author:      Juan Moises torrijos
Author URI:  https://moitorrijos.com/
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
*/

define( 'ICSCForms_PLUGIN', __FILE__ );

define( 'ICSCForms_PLUGIN_DIR', untrailingslashit( dirname( ICSCForms_PLUGIN ) ) );

require_once ICSCForms_PLUGIN_DIR . '/ics_forms_extra_user_profile_fields.php';
require_once ICSCForms_PLUGIN_DIR . '/ics_forms_save_extra_user_profile_fields.php';