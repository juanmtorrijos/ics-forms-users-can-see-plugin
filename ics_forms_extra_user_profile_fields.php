<?php

add_action( 'show_user_profile', 'ics_forms_extra_user_profile_fields' );
add_action( 'edit_user_profile', 'ics_forms_extra_user_profile_fields' );
add_action( 'user_new_form', 'ics_forms_extra_user_profile_fields' );

function ics_forms_extra_user_profile_fields( $user_id ) {
?>
	<table class="form-table">
	    <tr>
			<th scope="row"><?php _e( 'Forms User Can See' ) ?></th>
			<td><label for="icsc_classification"><input type="checkbox" name="icsc_classification" id="icsc_classification" value="1" checked  /> <?php _e( 'Classification' ); ?></label></td>
			<td><label for="icsc_load_lines"><input type="checkbox" name="icsc_load_lines" id="icsc_load_lines" value="1" checked /> <?php _e( 'Load Lines' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_solas"><input type="checkbox" name="icsc_solas" id="icsc_solas" value="1" checked /> <?php _e( 'SOLAS' ); ?></label></td>
			<td><label for="icsc_marpol"><input type="checkbox" name="icsc_marpol" id="icsc_marpol" value="1" checked /> <?php _e( 'MARPOL' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_tonnage"><input type="checkbox" name="icsc_tonnage" id="icsc_tonnage" value="1" checked /> <?php _e( 'Tonnage' ); ?></label></td>
			<td><label for="icsc_chemical_gas_tankers"><input type="checkbox" name="icsc_chemical_gas_tankers" id="icsc_chemical_gas_tankers" value="1" checked /> <?php _e( 'Chemical Tankers and Gas Carriers' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_mody_code"><input type="checkbox" name="icsc_mody_code" id="icsc_mody_code" value="1" checked /> <?php _e( 'MODU - Code' ); ?></label></td>
			<td><label for="icsc_ism_code"><input type="checkbox" name="icsc_ism_code" id="icsc_ism_code" value="1" checked /> <?php _e( 'ISM - Code' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_isps_code"><input type="checkbox" name="icsc_isps_code" id="icsc_isps_code" value="1" checked /> <?php _e( 'ISPS - Code' ); ?></label></td>
			<td><label for="icsc_u_500"><input type="checkbox" name="icsc_u_500" id="icsc_u_500" value="1" checked /> <?php _e( 'Vessels Under 500 GT' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_caribbean"><input type="checkbox" name="icsc_caribbean" id="icsc_caribbean" value="1" checked /> <?php _e( 'Caribbean Code' ); ?></label></td>
			<td><label for="icsc_fishing_vessels"><input type="checkbox" name="icsc_fishing_vessels" id="icsc_fishing_vessels" value="1" checked /> <?php _e( 'Fishing Vessels' ); ?></label></td>
		</tr>
		<tr>
			<th></th>
			<td><label for="icsc_ilo"><input type="checkbox" name="icsc_ilo" id="icsc_ilo" value="1" checked /> <?php _e( 'ILO Convention' ); ?></label></td>
			<td><label for="icsc_other_certs"><input type="checkbox" name="icsc_other_certs" id="icsc_other_certs" value="1" checked /> <?php _e( 'Other Certificates' ); ?></label></td>
		</tr>
	</table>
<?php
}